��            )   �      �     �     �     �     �     �     �  	   �     �     �  
   �     
          %     3  
   9     D  #   I     m     v     �  '   �  (   �  "   �  #        1     A     \     b     k  �  p           4     ;     B     ]     p     x     �     �     �     �     �     �     �     �     �  2        9     H     Y  ,   u  -   �  $   �  %   �       $   /     T  	   ]     g                                                
   	                                                                                      Change Background… Copy Cut Display Settings Empty trash Huge Icon size Large Move to Trash New Folder Open Open Desktop in Files Open Terminal Paste Properties Redo Set the size for the desktop icons. Settings Show in Files Show personal folder Show the personal folder in the desktop Show the personal folder in the desktop. Show the trash icon in the desktop Show the trash icon in the desktop. Show trash icon Size for the desktop icons Small Standard Undo Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2018-09-24 13:39+0200
PO-Revision-Date: 2018-09-24 13:40+0200
Last-Translator: Sergio Costas <rastersoft@gmail.com>
Language-Team: Español; Castellano <rastersoft@gmail.com>
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Gtranslator 2.91.7
 Cambiar el fondo... Copiar Cortar Configuración de pantalla Vaciar la papelera Inmenso Tamaño de los iconos Grande Mover a la papelera Nueva carpeta Abrir Abrir el escritorio en Files Abrir un terminal Pegar Propiedades Rehacer Establece el tamaño de los iconos del escritorio. Configuración Mostrar en Files Mostrar la carpeta personal Mostrar la carpeta personal en el escritorio Mostrar la carpeta personal en el escritorio. Mostrar la papelera en el escritorio Mostrar la papelera en el escritorio. Mostrar la papelera Tamaño de los iconos del escritorio Pequeño Estándar Deshacer 